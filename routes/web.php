<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LandingController@daftar');
Route::get('login', 'LandingController@masuk');

Route::get('dashboard', 'DashboardController@dashboard');
Route::post('1', 'DashboardController@form_1');
Route::post('2', 'DashboardController@form_2');
Route::post('3', 'DashboardController@form_3');
Route::post('4', 'DashboardController@form_4');
Route::post('5', 'DashboardController@form_5');
Route::post('6', 'DashboardController@form_6');

Route::post('daftar', 'DaftarController@postDaftar');
Route::post('masuk', 'MasukController@postMasuk');
Route::get('keluar', 'MasukController@keluar');

Route::get('fp', 'CetakController@fp');
Route::get('skm', 'CetakController@skm');
Route::get('ska', 'CetakController@ska');
Route::get('skc', 'CetakController@skc');
Route::get('sko', 'CetakController@sko');
Route::get('sio', 'CetakController@sio');
Route::get('skk', 'CetakController@skk');
