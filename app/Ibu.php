<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ibu extends Model
{
    protected $fillable = [
        'id', 'nama', 'tmpt_lahir', 'tgl_lahir', 'agama', 'pekerjaan', 'warganegara', 'alamat',
    ];
    public $timestamps = false;
}
