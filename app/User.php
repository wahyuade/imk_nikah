<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password', 'nama', 'jenis_kelamin', 'agama', 'pekerjaan', 'tmpt_lahir', 'tgl_lahir', 'warganegara', 'desa_kelurahan', 'kecamatan', 'kab_kota', 'kepala_ds_kel',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    public $timestamps = false;
}
