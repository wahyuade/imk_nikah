<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
class DaftarController extends Controller
{
    public function postDaftar(Request $req){
    	$nama = $req->nama;
    	$jenis_kelamin = $req->jenis_kelamin;
    	$agama = $req->agama;
    	$pekerjaan = $req->pekerjaan;
    	$tmpt_lahir = $req->tmpt_lahir;
    	$tgl_lahir = $req->tgl_lahir;
    	$warganegara = $req->warganegara;
    	$username = $req->username;
    	$password = $req->password;
    	$desa_kelurahan = $req->desa_kelurahan;
    	$kecamatan = $req->kecamatan;
    	$kab_kota = $req->kab_kota.$req->nama_kab_kota;
    	$kepala_ds_kel = $req->kepala_ds_kel;

    	$insert = new User;
    	$insert->nama = $nama;
    	$insert->username = $username;
    	$insert->password = Hash::make($password);
    	$insert->jenis_kelamin = $jenis_kelamin;
    	$insert->agama = $agama;
    	$insert->pekerjaan = $pekerjaan;
    	$insert->tmpt_lahir = $tmpt_lahir;
    	$insert->tgl_lahir = $tgl_lahir;
    	$insert->warganegara = $warganegara;
    	$insert->desa_kelurahan = $desa_kelurahan;
    	$insert->kecamatan = $kecamatan;
    	$insert->kab_kota = $kab_kota;
    	$insert->kepala_ds_kel = $kepala_ds_kel;
    	$insert->save();


        return redirect('login');
    }
}
