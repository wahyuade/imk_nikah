<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
class MasukController extends Controller
{
	protected $redirectTo = '/dashboard';
	public function postMasuk(Request $req){
		$username = $req->username;
		$password = $req->password;

		if (Auth::attempt(['username' => $username, 'password' => $password])) {
            return redirect()->intended('dashboard');
        }
        else{
        	$tab = "document.getElementById('b').click();";
        	return view('landing', compact('tab'));	
        }
	}
	public function keluar(){
		$username = Auth::User()->username_calon;
		Auth::logout();
		return redirect('login')->with('username', $username);
	}
}
