<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Ayah;
use App\Ibu;
use App\Wafat;
use App\Keterangan;
class DashboardController extends Controller
{
    public function __construct(){
	    $this->middleware('auth');
	}

	public function dashboard(){
		$data_ayah = Ayah::where('id', Auth::User()->id)->first();
		if($data_ayah != NULL){
			$ayah["nama"] = $data_ayah->nama;
			$ayah["tmpt_lahir"] = $data_ayah->tmpt_lahir;
			$ayah["tgl_lahir"] = $data_ayah->tgl_lahir;
			$ayah["warganegara"] = $data_ayah->warganegara;
			$ayah["agama"] = $data_ayah->agama;
			$ayah["pekerjaan"] = $data_ayah->pekerjaan;
			$ayah["alamat"] = $data_ayah->alamat;
		}
		else
			$ayah = NULL;

		$data_ibu = Ibu::where('id', Auth::User()->id)->first();
		if($data_ibu != NULL){
			$ibu["nama"] = $data_ibu->nama;
			$ibu["tmpt_lahir"] = $data_ibu->tmpt_lahir;
			$ibu["tgl_lahir"] = $data_ibu->tgl_lahir;
			$ibu["warganegara"] = $data_ibu->warganegara;
			$ibu["agama"] = $data_ibu->agama;
			$ibu["pekerjaan"] = $data_ibu->pekerjaan;
			$ibu["alamat"] = $data_ibu->alamat;
		}
		else
			$ibu = NULL;

		$data_calon = User::where('username_calon', Auth::User()->username)->first();
		if($data_calon != NULL){
			$calon["nama_mempelai"] = $data_calon->nama;
			$data_ayah_calon = Ayah::where('id', $data_calon->id)->first();
			$data_ibu_calon = Ibu::where('id', $data_calon->id)->first();
		}
		else{
			$calon = NULL;
			$data_ayah_calon = NULL;
			$data_ibu_calon = NULL;
		}

		if(Auth::User()->jenis_kelamin == "Laki-laki")
			$data_keterangan = Keterangan::where('username', Auth::User()->username."_".Auth::User()->username_calon)->first();
		else
			$data_keterangan = Keterangan::where('username', Auth::User()->username_calon."_".Auth::User()->username)->first();

		if($data_keterangan != NULL){
			$keterangan["hari"] = $data_keterangan->hari;
			$keterangan["tgl"] = $data_keterangan->tgl;
			$keterangan["jam"] = $data_keterangan->jam;
			$keterangan["mas_kawin"] = $data_keterangan->mas_kawin;
			$keterangan["bayar_mas_kawin"] = $data_keterangan->bayar_mas_kawin;
			$keterangan["tmpt"] = $data_keterangan->tmpt;
			$keterangan["nama_penerima_brks"] = $data_keterangan->nama_penerima_brks;
		}
		else
			$keterangan = NULL;
		return view('dashboard', compact('ayah', 'ibu', 'calon', 'keterangan','data_calon', 'data_ayah_calon', 'data_ibu_calon'));
	}

	public function form_1(Request $req){
		$wali = $req->wali;
		$alamat = $req->alamat;
		$mantan = $req->nama_pasangan_ex;
		$status = $req->status_kawin;
		User::where('id', Auth::User()->id)->update(['wali'=>$wali,'alamat'=>$alamat,'mantan'=>$mantan,'status'=>$status]);
		return "form_1 berhasil";
	}
	public function form_2(Request $req){
		$insert = new Ayah;
		$insert->id = Auth::User()->id;
		$insert->nama = $req->nama_ayah;
		$insert->tmpt_lahir = $req->tmpt_lahir_ayah;
		$insert->tgl_lahir = $req->tgl_lahir_ayah;
		$insert->warganegara = $req->warganegara_ayah;
		$insert->agama = $req->agama_ayah;
		$insert->pekerjaan = $req->pekerjaan_ayah;
		$insert->alamat = $req->alamat_ayah;
		$insert->save();
		return "form_2 Berhasil";
	}
	public function form_3(Request $req){
		$insert = new Ibu;
		$insert->id = Auth::User()->id;
		$insert->nama = $req->nama_ibu;
		$insert->tmpt_lahir = $req->tmpt_lahir_ibu;
		$insert->tgl_lahir = $req->tgl_lahir_ibu;
		$insert->warganegara = $req->warganegara_ibu;
		$insert->agama = $req->agama_ibu;
		$insert->pekerjaan = $req->pekerjaan_ibu;
		$insert->alamat = $req->alamat_ibu;
		$insert->save();
		return "form_3 Berhasil";
	}
	public function form_4(Request $req){
		$username_calon = $req->username_calon;
		if(User::where('username', $username_calon)->first() != NULL){
			User::where('id', Auth::User()->id)->update(['username_calon'=>$username_calon]);
			User::where('username', $username_calon)->update(['username_calon'=>Auth::User()->username]);
			return response()->json(['status'=>true, 'username'=>$username_calon]);;
		}
		else{
			return response()->json(['status'=>false, 'username'=>$username_calon]);
		}
	}
	public function form_5(Request $req){
		$insert = new Wafat;
		$insert->id = Auth::User()->id;
		$insert->nama = $req->nama_wafat;
		$insert->wali = $req->wali_wafat;
		$insert->tmpt_lahir = $req->tmpt_lahir_wafat;
		$insert->tgl_lahir = $req->tgl_lahir_wafat;
		$insert->tgl_meninggal = $req->tgl_meninggal_wafat;
		$insert->tmpt_meninggal = $req->tmpt_meninggal_wafat; 
		$insert->warganegara = $req->warganegara_wafat;
		$insert->agama = $req->agama_wafat;
		$insert->pekerjaan = $req->pekerjaan_wafat;
		$insert->alamat = $req->alamat_wafat;
		$insert->save();	
		return "form_5 Berhasil";
	}
	public function form_6(Request $req){
		$insert = new Keterangan();
		$insert->id = Auth::User()->id;
		if(Auth::User()->jenis_kelamin == "Laki-laki")
			$insert->username = Auth::User()->username."_".Auth::User()->username_calon;
		else
			$insert->username = Auth::User()->username_calon."_".Auth::User()->username;
		$insert->hari = $req->hari_nikah;
		$insert->tgl = $req->tanggal_nikah;
		$insert->jam = $req->jam_nikah;
		$insert->mas_kawin = $req->mas_kawin;
		$insert->bayar_mas_kawin = $req->bayar_mas_kawin;
		$insert->tmpt = $req->tempat_nikah;
		$insert->nama_penerima_brks = $req->penerima_berkas_nikah;
		$insert->tgl_terima = date("d-m-Y");
		$insert->save();
		return "form_6 Berhasil";
	}
}
