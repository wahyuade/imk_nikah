<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use Auth;
use App\User;
use App\Ayah;
use App\Ibu;
use App\Wafat;
use App\Keterangan;
class CetakController extends Controller
{
    private function tanggal(String $date){
        $data_user = User::where('id', Auth::User()->id)->first();
        if($date[5].$date[6] == "01"){
            return $date[8].$date[9]." Januari ".$date[0].$date[1].$date[2].$date[3];
        }elseif($date[5].$date[6] == "02"){
            return $date[8].$date[9]." Februari ".$date[0].$date[1].$date[2].$date[3];
        }
        elseif($date[5].$date[6] == "03"){
            return $date[8].$date[9]." Maret ".$date[0].$date[1].$date[2].$date[3];
        }
        elseif($date[5].$date[6] == "04"){
            return $date[8].$date[9]." April ".$date[0].$date[1].$date[2].$date[3];
        }
        elseif($date[5].$date[6] == "05"){
            return $date[8].$date[9]." Mei ".$date[0].$date[1].$date[2].$date[3];
        }
        elseif($date[5].$date[6] == "06"){
            return $date[8].$date[9]." Juni ".$date[0].$date[1].$date[2].$date[3];
        }
        elseif($date[5].$date[6] == "07"){
            return $date[8].$date[9]." Juli ".$date[0].$date[1].$date[2].$date[3];
        }
        elseif($date[5].$date[6] == "08"){
            return $date[8].$date[9]." Agustus ".$date[0].$date[1].$date[2].$date[3];
        }
        elseif($date[5].$date[6] == "09"){
            return $date[8].$date[9]." September ".$date[0].$date[1].$date[2].$date[3];
        }
        elseif($date[5].$date[6] == "10"){
            return $date[8].$date[9]." Oktober ".$date[0].$date[1].$date[2].$date[3];
        }
        elseif($date[5].$date[6] == "11"){
            return $date[8].$date[9]." November ".$date[0].$date[1].$date[2].$date[3];
        }
        elseif($date[5].$date[6] == "12"){
            return $date[8].$date[9]." Desember ".$date[0].$date[1].$date[2].$date[3];
        }
    }
    function fp(){
        if(Auth::User()->jenis_kelamin == "Laki-laki")
            $data_keterangan = Keterangan::where('username', Auth::User()->username."_".Auth::User()->username_calon)->first();
        else
            $data_keterangan = Keterangan::where('username', Auth::User()->username_calon."_".Auth::User()->username)->first();
        $data_user = User::where('id', Auth::User()->id)->first();
        $data_pasangan = User::where('username', Auth::User()->username_calon)->first();
        PDF::SetTitle('Formulir Pendaftaran');
        PDF::AddPage("P","",true);
        PDF::SetTopMargin(25);
        PDF::WriteHTML('Lampiran PMA : No. 11 Tahun 2007',true,false,false,false,'R');
        PDF::WriteHTML('- Pasal 5 ayat (2) -',true,false,false,false,'R');
        PDF::WriteHTML('<b>Model : N-7</b>',true,false,false,false,'R');
        PDF::WriteHTML('',true,false,false,false,'R');
        $tanggal_nkh = new CetakController;
        $tanggal_nikah = $tanggal_nkh->tanggal($data_keterangan->tgl);
        PDF::Write(0,$data_keterangan->hari.", ".$tanggal_nikah,'',false,'R');
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'Lampiran        :  6 Lembar','',false,'L');
        PDF::Ln();
        PDF::Write(0,'Perihal            :  Pemberitahuan Kehendak Nikah','',false,'L');
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'                                                                                                        Kepada Yth.','',false,'L');

        PDF::Ln();
        PDF::Write(0,'                                                                                                        Pegawai Pencatat nikah pada','',false,'L');
        PDF::Ln();
        PDF::Write(0,'                                                                                                        KUA Kecamatan/Pembantu PPN','',false,'L');
        PDF::Ln();
        PDF::Write(0,'                                                                                                        di '.$data_user->kecamatan,'',false,'L');
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,"Assalamu'alaikum wr. wb",'',false,'L');
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,"    Dengan ini kami memberitahukan bahwa kami bermaksud akan melangsungkan",'',false,'J');
        PDF::Ln();
        PDF::Write(0,"Pernikahan antara ",'',false,'L');
        PDF::Write(0," ",'',false,'L');
        PDF::WriteHTML('<b>'.$data_user->nama.'</b> dengan ',false,false,false,false,'L');
        PDF::Write(0," ",'',false,'L');
        PDF::WriteHTML('<b>'.$data_pasangan->nama.'</b> pada : ',false,false,false,false,'L');
        PDF::Ln();
        PDF::Write(0,'          hari          : '.$data_keterangan->hari,'',false,'L');
        PDF::Ln();
        PDF::Write(0,'          tanggal    : '.$tanggal_nikah,'',false,'L');
        PDF::Ln();
        PDF::Write(0,'          jam          : '.$data_keterangan->jam,'',false,'L');
        PDF::Ln();
        PDF::Write(0,'dengan mas kawin '.$data_keterangan->mas_kawin.' dibayar '.$data_keterangan->bayar_mas_kawin.' yang bertempat di '.$data_keterangan->tmpt,'',false,'L');
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'            Bersama ini kami melampirkan surat-surat yang diperlukan untuk diperiksa, sebagai berikut:','',false,'J');
        PDF::Ln();
        PDF::Write(0,'   1.   Surat Keterangan untuk menikah','',false,'L');
        PDF::Ln();
        PDF::Write(0,'   2.   Surat Keterangan Asal-Usul','',false,'L');
        PDF::Ln();
        PDF::Write(0,'   3.   Surat Persetujuan Mempelai','',false,'L');
        PDF::Ln();
        PDF::Write(0,'   4.   Surat Keterangan tentang Orang Tua','',false,'L');
        PDF::Ln();
        PDF::Write(0,'   5.   Surat Ijin Orang Tua','',false,'L');
        PDF::Ln();
        if($data_user->jenis_kelamin=="Laki-laki")
            PDF::Write(0,'   6.   Surat Keterangan Kematian Istri','',false,'L');
        else
            PDF::Write(0,'   6.   Surat Keterangan Kematian Suami','',false,'L');
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'        Hanya dapat dihadiri dan dicatat pelaksanaannya sesuai dengan keterntuan perundang-','',false,'J');
        PDF::Ln();
        PDF::Write(0,'undangan yang berlaku.','',false,'L');
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'Diterima tanggal '.$data_keterangan->tgl_terima,'',false,'L');
        PDF::Ln();
        PDF::Write(0,"                                                                                                        Wassalamu'alaikum wr.wb",'',false,'L');
        PDF::Ln();
        PDF::Write(0,"                                                                                                        Yang memberitahukan,",'',false,'L');
        PDF::Ln();
        PDF::Write(0,"  Yang Menerima                                                                                 Calon Mempelai",'',false,'L');
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,$data_keterangan->nama_penerima_brks,'',false,'L');
        PDF::Write(0,''.$data_user->nama.'                  ','',false,'R');
        PDF::Output('Formulir Pendaftaran.pdf');
    }
    function skm(){
        $data_user = User::where('id', Auth::User()->id)->first();
        if(Auth::User()->jenis_kelamin == "Laki-laki")
            $data_keterangan = Keterangan::where('username', Auth::User()->username."_".Auth::User()->username_calon)->first();
        else
            $data_keterangan = Keterangan::where('username', Auth::User()->username_calon."_".Auth::User()->username)->first();
        $kantor = '
            <table>
                <tr>
                    <td width="200px">KANTOR DESA / KELURAHAN</td>
                    <td width="10px">:</td>
                    <td width="120px">'.$data_user->desa_kelurahan.'</td>
                    
                </tr>
                <tr>
                    <td>KECAMATAN</td>
                    <td>:</td>
                    <td>'.$data_user->kecamatan.'</td>
                  
                </tr>
                <tr>
                    <td>KABUPATEN / KOTA</td>
                    <td>:</td>
                    <td>'.$data_user->kab_kota.'</td>
                   
                </tr>
            </table>';
        $nomor = 'Nomor : '.'SKK/'.$data_user->kecamatan.'/'.rand(0,1000);
        PDF::SetTitle('Surat Keterangan Menikah');
        PDF::AddPage("P","",true);
        PDF::SetTopMargin(25);
        PDF::WriteHTML('<b>Model : N-1</b>',true,false,false,false,'R');
        PDF::WriteHTML($kantor,true,false,false,false,'L');
        PDF::WriteHTML('<b><u>SURAT KETERANGAN UNTUK NIKAH</u></b>',true,false,false,false,'C');
        PDF::WriteHTML($nomor,true,false,false,false,'C');
        PDF::Ln();
        PDF::WriteHTML('Yang bertanda tangan dibawah ini menerangkan dengan sesungguhnya bahwa : ',true,false,false,false,'L');
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'     1. Nama Lengkap dan Alias       : ');
        PDF::Write(0,$data_user->nama);
        PDF::Ln();
        PDF::Write(0,'     2. Jenis Kelamin                         : ');
        PDF::Write(0,$data_user->jenis_kelamin);
        PDF::Ln();
        PDF::Write(0,'     3. Tempat tanggal Lahir             : ');
        $td = new CetakController;
        PDF::Write(0,$data_user->tmpt_lahir.", ".$td->tanggal($data_user->tgl_lahir));
        PDF::Ln();
        PDF::Write(0,'     4. Warga Negara                        : ');
        PDF::Write(0,$data_user->warganegara);
        PDF::Ln();
        PDF::Write(0,'     5. Agama                                    : ');
        PDF::Write(0,$data_user->agama);
        PDF::Ln();
        PDF::Write(0,'     6. Pekerjaan                               : ');
        PDF::Write(0,$data_user->pekerjaan); 
        PDF::Ln(); 
        PDF::Write(0,'     7. Tempat tinggal                       : ');
        PDF::Write(0,$data_user->alamat);
        PDF::Ln(); 
        if($data_user->jenis_kelamin=="Laki-laki")
            PDF::Write(0,'     8. Bin                                          : ');
        else
            PDF::Write(0,'     8. Binti                                        : ');
        PDF::Write(0,$data_user->wali);
        PDF::Ln(); 
        PDF::Write(0,'     9. Status Perkawinan                 : ');
        PDF::Write(0,$data_user->status);
        if($data_user->jenis_kelamin=="Laki-laki")
            if($data_user->status!="Jejaka"){
                PDF::Ln(); 
                PDF::Write(0,'     10. Nama Istri Terdahulu            : ');
            }else{
                PDF::Ln(); 
                PDF::Write(0,'     10. Nama Istri Terdahulu            : -');
            }
        if($data_user->jenis_kelamin=="Perempuan")
            if($data_user->status!="Perawan"){
                PDF::Ln(); 
                PDF::Write(0,'     10. Nama Suami Terdahulu       : ');   
            }else{
                PDF::Ln(); 
                PDF::Write(0,'     10. Nama Suami Terdahulu       : -');                   
            }
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'              Demikianlah, surat keterangan ini dibuat dengan mengingat sumpah jabatan dan untuk dipergunakan seperlunya.');
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        $surat = $data_keterangan->tmpt.', '.$data_keterangan->tgl_terima;
        $kantor_ttd = 'Kantor Desa / Kelurahan '.$data_user->desa_kelurahan;
        PDF::WriteHTML($surat,true,false,false,false,'R');
        PDF::WriteHTML($kantor_ttd,true,false,false,false,'R');
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,$data_user->nama.'           ','',false,'R');
        PDF::Output('Surat Keterangan untuk nikah.pdf');
    }
    function ska(){
    	$data_user = User::where('id', Auth::User()->id)->first();
        $data_ayah = Ayah::where('id', Auth::User()->id)->first();
        $data_ibu = Ibu::where('id', Auth::User()->id)->first();
        if(Auth::User()->jenis_kelamin == "Laki-laki")
            $data_keterangan = Keterangan::where('username', Auth::User()->username."_".Auth::User()->username_calon)->first();
        else
            $data_keterangan = Keterangan::where('username', Auth::User()->username_calon."_".Auth::User()->username)->first();
        $kantor = '
            <table>
                <tr>
                    <td width="200px">KANTOR DESA / KELURAHAN</td>
                    <td width="10px">:</td>
                    <td width="120px">'.$data_user->desa_kelurahan.'</td>
                    
                </tr>
                <tr>
                    <td>KECAMATAN</td>
                    <td>:</td>
                    <td>'.$data_user->kecamatan.'</td>
                  
                </tr>
                <tr>
                    <td>KABUPATEN / KOTA</td>
                    <td>:</td>
                    <td>'.$data_user->kab_kota.'</td>
                   
                </tr>
            </table>';
        $nomor = 'Nomor : '.'SKK/'.$data_user->kecamatan.'/'.rand(0,1000);
        PDF::SetTitle('Surat Keterangan Asal-Usul');
        PDF::AddPage("P","",true);
        PDF::SetTopMargin(19);
        PDF::WriteHTML('<b>Model : N-2</b>',true,false,false,false,'R');
        PDF::WriteHTML($kantor,true,false,false,false,'L');
        PDF::WriteHTML('<b><u>SURAT KETERANGAN ASAL - USUL</u></b>',true,false,false,false,'C');
        PDF::WriteHTML($nomor,true,false,false,false,'C');
        PDF::Ln();
        PDF::WriteHTML('Yang bertanda tangan dibawah ini menerangkan dengan sesungguhnya bahwa : ',true,false,false,false,'L');
        PDF::Ln();
        PDF::Write(0,'I      1. Nama Lengkap dan Alias       : ');
        PDF::Write(0,$data_user["nama"]);
        PDF::Ln();
        PDF::Write(0,'       2. Tempat tanggal Lahir             : ');
        $td = new CetakController;
        PDF::Write(0,$data_user["tmpt_lahir"].", ".$td->tanggal($data_user["tgl_lahir"]));
        PDF::Ln();
        PDF::Write(0,'       3. Warga Negara                        : ');
        PDF::Write(0,$data_user["warganegara"]);
        PDF::Ln();
        PDF::Write(0,'       4. Agama                                    : ');
        PDF::Write(0,$data_user["agama"]);
        PDF::Ln();
        PDF::Write(0,'       5. Pekerjaan                               : ');
        PDF::Write(0,$data_user["pekerjaan"]);
        PDF::Ln();
        PDF::Write(0,'       6. Tempat tinggal                       : ');
        PDF::Write(0,$data_user["alamat"]);
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'       adalah benar ayah kandung dan ibu kandung dari seorang :');
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'II     1. Nama Lengkap dan Alias       : ');
        PDF::Write(0,$data_ayah["nama"]);
        PDF::Ln();
        PDF::Write(0,'       2. Tempat tanggal Lahir             : ');
        $td = new CetakController;
        PDF::Write(0,$data_ayah["tmpt_lahir"].", ".$td->tanggal($data_ayah["tgl_lahir"]));
        PDF::Ln();
        PDF::Write(0,'       3. Warga Negara                        : ');
        PDF::Write(0,$data_ayah["warganegara"]);
        PDF::Ln();
        PDF::Write(0,'       4. Agama                                    : ');
        PDF::Write(0,$data_ayah["agama"]);
        PDF::Ln();
        PDF::Write(0,'       5. Pekerjaan                               : ');
        PDF::Write(0,$data_ayah["pekerjaan"]);
        PDF::Ln();
        PDF::Write(0,'       6. Tempat tinggal                       : ');
        PDF::Write(0,$data_ayah["alamat"]);
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'       dengan seorang wanita :');
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'III    1. Nama Lengkap dan Alias       : ');
        PDF::Write(0,$data_ibu["nama"]);
        PDF::Ln();
        PDF::Write(0,'       2. Tempat tanggal Lahir             : ');
        $td = new CetakController;
        PDF::Write(0,$data_ibu["tmpt_lahir"].", ".$td->tanggal($data_ibu["tgl_lahir"]));
        PDF::Ln();
        PDF::Write(0,'       3. Warga Negara                        : ');
        PDF::Write(0,$data_ibu["warganegara"]);
        PDF::Ln();
        PDF::Write(0,'       4. Agama                                    : ');
        PDF::Write(0,$data_ibu["agama"]);
        PDF::Ln();
        PDF::Write(0,'       5. Pekerjaan                               : ');
        PDF::Write(0,$data_ibu["pekerjaan"]);
        PDF::Ln();
        PDF::Write(0,'       6. Tempat tinggal                       : ');
        PDF::Write(0,$data_ibu["alamat"]);
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'              Demikianlah, surat keterangan ini dibuat dengan mengingat sumpah jabatan dan untuk dipergunakan seperlunya.');
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        $surat = $data_keterangan->tmpt.', '.$data_keterangan->tgl_terima;
        $kantor_ttd = 'Kantor Desa / Kelurahan '.$data_user->desa_kelurahan;
        PDF::WriteHTML($surat,true,false,false,false,'R');
        PDF::WriteHTML($kantor_ttd,true,false,false,false,'R');
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,$data_user->nama.'           ','',false,'R');
        PDF::Output('Surat Keterangan asal-usul.pdf');
    }
    function skc(){
        $data_user = User::where('id', Auth::User()->id)->first();
        $data_pasangan = User::where('username', Auth::User()->username_calon)->first();
        if(Auth::User()->jenis_kelamin == "Laki-laki")
            $data_keterangan = Keterangan::where('username', Auth::User()->username."_".Auth::User()->username_calon)->first();
        else
            $data_keterangan = Keterangan::where('username', Auth::User()->username_calon."_".Auth::User()->username)->first();
        PDF::SetTitle('Surat Persetujuan Mempelai');
        PDF::AddPage("P","",true);
        PDF::SetTopMargin(20);
        PDF::WriteHTML('Lampiran PMA : No. 11 Tahun 2007',true,false,false,false,'R');
        PDF::WriteHTML('- Pasal 5 ayat (2) huruf c',true,false,false,false,'R');
        PDF::WriteHTML('<b>Model : N-3</b>',true,false,false,false,'R');
        PDF::WriteHTML('<br><br><br><br><br><br><b><u>SURAT PERSETUJUAN MEMPELAI</u></b><br>',true,false,false,false,'C');
        PDF::WriteHTML('Yang bertanda tangan dibawah ini : ',true,false,'L');
        PDF::Ln();
        PDF::WriteHTML('<b>I. Calon Suami</b> : ',true,false,'L');
        PDF::Write(0,'       1. Nama Lengkap dan Alias       : ');
        PDF::Write(0,$data_user["nama"]);
        PDF::Ln();
        if($data_user->jenis_kelamin=="Laki-laki")
            PDF::Write(0,'       2. Bin                                          : ');
        else
            PDF::Write(0,'       2. Binti                                        : ');
        PDF::Write(0,$data_user["wali"]);
        PDF::Ln();
        PDF::Write(0,'       3. Tempat tanggal Lahir             : ');
        $td = new CetakController;
        PDF::Write(0,$data_user["tmpt_lahir"].", ".$td->tanggal($data_user["tgl_lahir"]));
        PDF::Ln();
        PDF::Write(0,'       4. Warga Negara                        : ');
        PDF::Write(0,$data_user["warganegara"]);
        PDF::Ln();
        PDF::Write(0,'       5. Agama                                    : ');
        PDF::Write(0,$data_user["agama"]);
        PDF::Ln();
        PDF::Write(0,'       6. Pekerjaan                               : ');
        PDF::Write(0,$data_user["pekerjaan"]);
        PDF::Ln();
        PDF::Write(0,'       7. Tempat tinggal                       : ');
        PDF::Write(0,$data_user["alamat"]);
        PDF::Ln();
        PDF::Ln();
        PDF::WriteHTML('<b>II. Calon Istri</b> : ',true,false,'L');
        PDF::Write(0,'       1. Nama Lengkap dan Alias       : ');
        PDF::Write(0,$data_pasangan["nama"]);
        PDF::Ln();
        if($data_pasangan->jenis_kelamin=="Laki-laki")
            PDF::Write(0,'       2. Bin                                          : ');
        else
            PDF::Write(0,'       2. Binti                                        : ');
        PDF::Write(0,$data_pasangan["wali"]);
        PDF::Ln();
        PDF::Write(0,'       3. Tempat tanggal Lahir             : ');
        $td = new CetakController;
        PDF::Write(0,$data_pasangan["tmpt_lahir"].", ".$td->tanggal($data_pasangan["tgl_lahir"]));
        PDF::Ln();
        PDF::Write(0,'       4. Warga Negara                        : ');
        PDF::Write(0,$data_pasangan["warganegara"]);
        PDF::Ln();
        PDF::Write(0,'       5. Agama                                    : ');
        PDF::Write(0,$data_pasangan["agama"]);
        PDF::Ln();
        PDF::Write(0,'       6. Pekerjaan                               : ');
        PDF::Write(0,$data_pasangan["pekerjaan"]);
        PDF::Ln();
        PDF::Write(0,'       7. Tempat tinggal                       : ');
        PDF::Write(0,$data_pasangan["alamat"]);
        PDF::Ln();
        PDF::Ln();
        PDF::WriteHTML('Menyatakan dengan sesungguhnya bahwa atas dasar suka rela, dengan kesadaran sendiri, tanpa',true,false,'J');        
        PDF::WriteHTML('paksaan dari siapapun juga, setuju untuk melangsungkan pernikahan.',true,false,'L');
        PDF::Ln();
        PDF::Ln();
        PDF::WriteHTML('Demikian surat persetujuan ini dibuat untuk digunakan seperlunya.',true,false,'L');
        $surat = $data_keterangan->tmpt.', '.$data_keterangan->tgl_terima;
        $kantor_ttd = 'Kantor Desa / Kelurahan '.$data_user->desa_kelurahan;
        PDF::Ln();
        PDF::WriteHTML($surat.'<br><br>',true,false,false,false,'R');
        PDF::Write(0,'                 I. Calon Suami','',false,'L');
        PDF::Write(0,'II. Calon Istri                      ','',false,'R');
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'           '.$data_user["nama"],'',false,'L');
        PDF::Write(0,$data_pasangan["nama"].'           ','',false,'R');
        PDF::Output('Surat Persetujuan Mempelai.pdf');
    }
    function sko(){
        $data_user = User::where('id', Auth::User()->id)->first();
        $data_ayah = Ayah::where('id', Auth::User()->id)->first();
        $data_ibu = Ibu::where('id', Auth::User()->id)->first();
        if(Auth::User()->jenis_kelamin == "Laki-laki")
            $data_keterangan = Keterangan::where('username', Auth::User()->username."_".Auth::User()->username_calon)->first();
        else
            $data_keterangan = Keterangan::where('username', Auth::User()->username_calon."_".Auth::User()->username)->first();
        $kantor = '
            <table>
                <tr>
                    <td width="200px">KANTOR DESA / KELURAHAN</td>
                    <td width="10px">:</td>
                    <td width="120px">'.$data_user->desa_kelurahan.'</td>
                    
                </tr>
                <tr>
                    <td>KECAMATAN</td>
                    <td>:</td>
                    <td>'.$data_user->kecamatan.'</td>
                  
                </tr>
                <tr>
                    <td>KABUPATEN / KOTA</td>
                    <td>:</td>
                    <td>'.$data_user->kab_kota.'</td>
                   
                </tr>
            </table>';
        $nomor = 'Nomor : '.'SKK/'.$data_user->kecamatan.'/'.rand(0,1000);
        PDF::SetTitle('Surat Keterangan tentang Orang Tua');
        PDF::AddPage("P","",true);
        PDF::SetTopMargin(19);
        PDF::WriteHTML('<b>Model : N-4</b>',true,false,false,false,'R');
        PDF::WriteHTML($kantor,true,false,false,false,'L');
        PDF::WriteHTML('<b><u>SURAT KETERANGAN TENTANG ORANG TUA</u></b>',true,false,false,false,'C');
        PDF::WriteHTML($nomor,true,false,false,false,'C');
        PDF::Ln();
        PDF::WriteHTML('Yang bertanda tangan dibawah ini menerangkan dengan sesungguhnya bahwa : ',true,false,false,false,'L');
        PDF::Ln();
        PDF::Write(0,'I      1. Nama Lengkap dan Alias       : ');
        PDF::Write(0,$data_ayah["nama"]);
        PDF::Ln();
        PDF::Write(0,'       2. Tempat tanggal Lahir             : ');
        $td = new CetakController;
        PDF::Write(0,$data_ayah["tmpt_lahir"].", ".$td->tanggal($data_ayah["tgl_lahir"]));
        PDF::Ln();
        PDF::Write(0,'       3. Warga Negara                        : ');
        PDF::Write(0,$data_ayah["warganegara"]);
        PDF::Ln();
        PDF::Write(0,'       4. Agama                                    : ');
        PDF::Write(0,$data_ayah["agama"]);
        PDF::Ln();
        PDF::Write(0,'       5. Pekerjaan                               : ');
        PDF::Write(0,$data_ayah["pekerjaan"]);
        PDF::Ln();
        PDF::Write(0,'       6. Tempat tinggal                       : ');
        PDF::Write(0,$data_ayah["alamat"]);
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'II     1. Nama Lengkap dan Alias       : ');
        PDF::Write(0,$data_ibu["nama"]);
        PDF::Ln();
        PDF::Write(0,'       2. Tempat tanggal Lahir             : ');
        $td = new CetakController;
        PDF::Write(0,$data_ibu["tmpt_lahir"].", ".$td->tanggal($data_ibu["tgl_lahir"]));
        PDF::Ln();
        PDF::Write(0,'       3. Warga Negara                        : ');
        PDF::Write(0,$data_ibu["warganegara"]);
        PDF::Ln();
        PDF::Write(0,'       4. Agama                                    : ');
        PDF::Write(0,$data_ibu["agama"]);
        PDF::Ln();
        PDF::Write(0,'       5. Pekerjaan                               : ');
        PDF::Write(0,$data_ibu["pekerjaan"]);
        PDF::Ln();
        PDF::Write(0,'       6. Tempat tinggal                       : ');
        PDF::Write(0,$data_ibu["alamat"]);
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'adalah benar ayah kandung dan ibu kandung dari seorang :');
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'       1. Nama Lengkap dan Alias       : ');
        PDF::Write(0,$data_user["nama"]);
        PDF::Ln();
        PDF::Write(0,'       2. Tempat tanggal Lahir             : ');
        $td = new CetakController;
        PDF::Write(0,$data_user["tmpt_lahir"].", ".$td->tanggal($data_user["tgl_lahir"]));
        PDF::Ln();
        PDF::Write(0,'       3. Warga Negara                        : ');
        PDF::Write(0,$data_user["warganegara"]);
        PDF::Ln();
        PDF::Write(0,'       4. Jenis Kelamin                         : ');
        PDF::Write(0,$data_user["jenis_kelamin"]);
        PDF::Ln();
        PDF::Write(0,'       5. Agama                                    : ');
        PDF::Write(0,$data_user["agama"]);
        PDF::Ln();
        PDF::Write(0,'       6. Pekerjaan                               : ');
        PDF::Write(0,$data_user["pekerjaan"]);
        PDF::Ln();
        PDF::Write(0,'       7. Tempat tinggal                       : ');
        PDF::Write(0,$data_user["alamat"]);
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'              Demikianlah, surat keterangan ini dibuat dengan mengingat sumpah jabatan dan untuk dipergunakan seperlunya.');
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        $surat = $data_keterangan->tmpt.', '.$data_keterangan->tgl_terima;
        $kantor_ttd = 'Kantor Desa / Kelurahan '.$data_user->desa_kelurahan;
        PDF::WriteHTML($surat,true,false,false,false,'R');
        PDF::WriteHTML($kantor_ttd,true,false,false,false,'R');
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,$data_user->nama.'           ','',false,'R');
        PDF::Output('Surat Keterangan tentang Orang Tua.pdf');	
    }
    function sio(){
        $data_ayah = Ayah::where('id', Auth::User()->id)->first();
        $data_ibu = Ibu::where('id', Auth::User()->id)->first();
        $data_user = User::where('id', Auth::User()->id)->first();
        $data_pasangan = User::where('username', Auth::User()->username_calon)->first();
        if(Auth::User()->jenis_kelamin == "Laki-laki")
            $data_keterangan = Keterangan::where('username', Auth::User()->username."_".Auth::User()->username_calon)->first();
        else
            $data_keterangan = Keterangan::where('username', Auth::User()->username_calon."_".Auth::User()->username)->first();
        PDF::SetTitle('Surat Keterangan tentang Orang Tua');
        PDF::AddPage("P","",true);
    	PDF::WriteHTML('<b>Model : N-5</b>',true,false,false,false,'R');
        PDF::WriteHTML('<b><u>SURAT IJIN ORANG TUA</u></b>',true,false,false,false,'C');
        PDF::Ln();
        PDF::WriteHTML('Yang bertanda tangan dibawah ini : ',true,false,false,false,'L');
        PDF::Ln();
        PDF::Write(0,'I      1. Nama Lengkap dan Alias       : ');
        PDF::Write(0,$data_ayah["nama"]);
        PDF::Ln();
        PDF::Write(0,'       2. Tempat tanggal Lahir             : ');
        $td = new CetakController;
        PDF::Write(0,$data_ayah["tmpt_lahir"].", ".$td->tanggal($data_ayah["tgl_lahir"]));
        PDF::Ln();
        PDF::Write(0,'       3. Warga Negara                        : ');
        PDF::Write(0,$data_ayah["warganegara"]);
        PDF::Ln();
        PDF::Write(0,'       4. Agama                                    : ');
        PDF::Write(0,$data_ayah["agama"]);
        PDF::Ln();
        PDF::Write(0,'       5. Pekerjaan                               : ');
        PDF::Write(0,$data_ayah["pekerjaan"]);
        PDF::Ln();
        PDF::Write(0,'       6. Tempat tinggal                       : ');
        PDF::Write(0,$data_ayah["alamat"]);
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'II     1. Nama Lengkap dan Alias       : ');
        PDF::Write(0,$data_ibu["nama"]);
        PDF::Ln();
        PDF::Write(0,'       2. Tempat tanggal Lahir             : ');
        $td = new CetakController;
        PDF::Write(0,$data_ibu["tmpt_lahir"].", ".$td->tanggal($data_ibu["tgl_lahir"]));
        PDF::Ln();
        PDF::Write(0,'       3. Warga Negara                        : ');
        PDF::Write(0,$data_ibu["warganegara"]);
        PDF::Ln();
        PDF::Write(0,'       4. Agama                                    : ');
        PDF::Write(0,$data_ibu["agama"]);
        PDF::Ln();
        PDF::Write(0,'       5. Pekerjaan                               : ');
        PDF::Write(0,$data_ibu["pekerjaan"]);
        PDF::Ln();
        PDF::Write(0,'       6. Tempat tinggal                       : ');
        PDF::Write(0,$data_ibu["alamat"]);
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'adalah ayah kandung dan ibu kandung dari :');
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'       1. Nama Lengkap dan Alias       : ');
        PDF::Write(0,$data_user["nama"]);
        PDF::Ln();
        PDF::Write(0,'       2. Tempat tanggal Lahir             : ');
        $td = new CetakController;
        PDF::Write(0,$data_user["tmpt_lahir"].", ".$td->tanggal($data_user["tgl_lahir"]));
        PDF::Ln();
        PDF::Write(0,'       3. Warga Negara                        : ');
        PDF::Write(0,$data_user["warganegara"]);
        PDF::Ln();
        PDF::Write(0,'       4. Agama                                    : ');
        PDF::Write(0,$data_user["agama"]);
        PDF::Ln();
        PDF::Write(0,'       5. Pekerjaan                               : ');
        PDF::Write(0,$data_user["pekerjaan"]);
        PDF::Ln();
        PDF::Write(0,'       6. Tempat tinggal                       : ');
        PDF::Write(0,$data_user["alamat"]);
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'memberikan ijin kepadanya untuk melakukan pernikahan dengan :');
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'       1. Nama Lengkap dan Alias       : ');
        PDF::Write(0,$data_pasangan["nama"]);
        PDF::Ln();
        PDF::Write(0,'       2. Tempat tanggal Lahir             : ');
        $td = new CetakController;
        PDF::Write(0,$data_pasangan["tmpt_lahir"].", ".$td->tanggal($data_pasangan["tgl_lahir"]));
        PDF::Ln();
        PDF::Write(0,'       3. Warga Negara                        : ');
        PDF::Write(0,$data_pasangan["warganegara"]);
        PDF::Ln();
        PDF::Write(0,'       4. Agama                                    : ');
        PDF::Write(0,$data_pasangan["agama"]);
        PDF::Ln();
        PDF::Write(0,'       5. Pekerjaan                               : ');
        PDF::Write(0,$data_pasangan["pekerjaan"]);
        PDF::Ln();
        PDF::Write(0,'       6. Tempat tinggal                       : ');
        PDF::Write(0,$data_pasangan["alamat"]);
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'Demikianlah surat ijin ini dibuat dengan kesadaran tanpa ada paksaan dari siapapun juga dan ','',false,'J');
        PDF::Ln();
        PDF::Write(0,'untuk dipergunakan seperlunya.','',false,'L');
        PDF::Ln();
        $surat = $data_keterangan->tmpt.', '.$data_keterangan->tgl_terima;
        PDF::Ln();
        PDF::WriteHTML($surat.'<br>',true,false,false,false,'R');
        PDF::Write(0,'                      I. Ayah','',false,'L');
        PDF::Write(0,'II. Ibu                           ','',false,'R');
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'                '.$data_ayah["nama"],'',false,'L');
        PDF::Write(0,$data_ibu["nama"].'                ','',false,'R');
        PDF::Output('Surat ijin orang tua.pdf');
    }
    function skk(){
        //get data
        $data_waf = Wafat::where('id', Auth::User()->id)->first();
        if(Auth::User()->jenis_kelamin == "Laki-laki")
            $data_keterangan = Keterangan::where('username', Auth::User()->username."_".Auth::User()->username_calon)->first();
        else
            $data_keterangan = Keterangan::where('username', Auth::User()->username_calon."_".Auth::User()->username)->first();
        $data_user = User::where('id', Auth::User()->id)->first();
        //get data

        if($data_waf != NULL){
            $data_wafat["nama"] = $data_waf->nama;
            $data_wafat["wali"] = $data_waf->wali;
            $data_wafat["tmpt_lahir"] = $data_waf->tmpt_lahir;
            $data_wafat["tgl_lahir"] = $data_waf->tgl_lahir;
            $data_wafat["tgl_meninggal"] = $data_waf->tgl_meninggal;
            $data_wafat["tmpt_meninggal"] = $data_waf->tmpt_meninggal;
            $data_wafat["warganegara"] = $data_waf->warganegara;
            $data_wafat["agama"] = $data_waf->agama;
            $data_wafat["pekerjaan"] = $data_waf->pekerjaan;
            $data_wafat["alamat"] = $data_waf->alamat;
        }else{
            $data_wafat["nama"] = "-";
            $data_wafat["wali"] = "-";
            $data_wafat["tmpt_lahir"] = "-";
            $data_wafat["tgl_lahir"] = "-";
            $data_wafat["tgl_meninggal"] = "-";
            $data_wafat["tmpt_meninggal"] = "-";
            $data_wafat["warganegara"] = "-";
            $data_wafat["agama"] = "-";
            $data_wafat["pekerjaan"] = "-";
            $data_wafat["alamat"] = "-";
        }
        $kantor = '
            <table>
                <tr>
                    <td width="200px">KANTOR DESA / KELURAHAN</td>
                    <td width="10px">:</td>
                    <td width="120px">'.$data_user->desa_kelurahan.'</td>
                    
                </tr>
                <tr>
                    <td>KECAMATAN</td>
                    <td>:</td>
                    <td>'.$data_user->kecamatan.'</td>
                  
                </tr>
                <tr>
                    <td>KABUPATEN / KOTA</td>
                    <td>:</td>
                    <td>'.$data_user->kab_kota.'</td>
                   
                </tr>
            </table>';
        $nomor = 'Nomor : '.'SKK/'.$data_user->kecamatan.'/'.rand(0,1000);

        PDF::SetTitle('Surat Keterangan Kematian');
        PDF::AddPage("P","",true);
        PDF::SetTopMargin(25);
        PDF::WriteHTML('Lampiran PMA : No. 11 Tahun 2007',true,false,false,false,'R');
        PDF::WriteHTML('- Pasal 5 ayat (2) huruf k',true,false,false,false,'R');
        PDF::WriteHTML('<b>Model : N-6</b>',true,false,false,false,'R');
        PDF::WriteHTML($kantor,true,false,false,false,'L');
        if($data_user->jenis_kelamin=="Laki-laki")
            PDF::WriteHTML('<b><u>SURAT KETERANGAN KEMATIAN ISTRI</u></b>',true,false,false,false,'C');
        else
            PDF::WriteHTML('<b><u>SURAT KETERANGAN KEMATIAN SUAMI</u></b>',true,false,false,false,'C');
        PDF::WriteHTML($nomor,true,false,false,false,'C');
        PDF::Ln();
        PDF::WriteHTML('Yang bertanda tangan dibawah ini menerangkan dengan sesungguhnya bahwa : ',true,false,false,false,'L');
        PDF::Ln();
        PDF::Write(0,'I     1. Nama Lengkap dan Alias       : ');
        PDF::Write(0,$data_wafat["nama"]);
        PDF::Ln();
        if($data_user->jenis_kelamin=="Laki-laki")
            PDF::Write(0,'      2. Bin                                          : ');
        else
            PDF::Write(0,'      2. Binti                                          : ');
        PDF::Write(0,$data_wafat["wali"]);
        PDF::Ln();
        PDF::Write(0,'      3. Tempat tanggal Lahir             : ');
        PDF::Write(0,$data_wafat["tmpt_lahir"].", ".$data_wafat["tgl_lahir"]);
        PDF::Ln();
        PDF::Write(0,'      4. Warga Negara                        : ');
        PDF::Write(0,$data_wafat["warganegara"]);
        PDF::Ln();
        PDF::Write(0,'      5. Agama                                    : ');
        PDF::Write(0,$data_wafat["agama"]);
        PDF::Ln();
        PDF::Write(0,'      6. Pekerjaan terakhir                  : ');
        PDF::Write(0,$data_wafat["pekerjaan"]);
        PDF::Ln();
        PDF::Write(0,'      7. Tempat tinggal terakhir          : ');
        PDF::Write(0,$data_wafat["alamat"]);
        PDF::Ln();
        PDF::Ln();
        PDF::WriteHTML('telah meninggal dunia pada tanggal : ',false,false,false,false,'L');
        PDF::Write(0,$data_wafat["tgl_meninggal"]);
        PDF::Ln();
        PDF::Write(0,'di                                                      : ');
        PDF::Write(0,$data_wafat["tmpt_meninggal"]);
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'II     1. Nama Lengkap dan Alias       : ');
        PDF::Write(0,$data_user["nama"]);
        PDF::Ln();
        if($data_user->jenis_kelamin=="Laki-laki")
            PDF::Write(0,'       2. Binti                                        : ');
        else
            PDF::Write(0,'       2. Bin                                          : ');
        PDF::Write(0,$data_user["wali"]);
        PDF::Ln();
        PDF::Write(0,'       3. Tempat tanggal Lahir             : ');
        PDF::Write(0,$data_user["tmpt_lahir"].", ".$data_user["tgl_lahir"]);
        PDF::Ln();
        PDF::Write(0,'       4. Warga Negara                        : ');
        PDF::Write(0,$data_user["warganegara"]);
        PDF::Ln();
        PDF::Write(0,'       5. Agama                                    : ');
        PDF::Write(0,$data_user["agama"]);
        PDF::Ln();
        PDF::Write(0,'       6. Pekerjaan                               : ');
        PDF::Write(0,$data_user["pekerjaan"]);
        PDF::Ln();
        PDF::Write(0,'       7. Tempat tinggal                       : ');
        PDF::Write(0,$data_user["alamat"]);
        PDF::Ln();
        PDF::Ln();
        if($data_user->jenis_kelamin=="Laki-laki")
            PDF::WriteHTML('adalah istri yang telah meninggal tersebut diatas.',false,false,false,false,'L');
        else
            PDF::WriteHTML('adalah suami yang telah meninggal tersebut diatas.',false,false,false,false,'L');
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,'              Demikianlah, surat keterangan ini dibuat dengan mengingat sumpah jabatan dan untuk dipergunakan seperlunya.');
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        $surat = $data_keterangan->tmpt.', '.$data_keterangan->tgl_terima;
        $kantor_ttd = 'Kantor Desa / Kelurahan '.$data_user->desa_kelurahan;
        PDF::WriteHTML($surat,true,false,false,false,'R');
        PDF::WriteHTML($kantor_ttd,true,false,false,false,'R');
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Ln();
        PDF::Write(0,$data_user->nama.'           ','',false,'R');
        PDF::Output('Surat Keterangan Kematian.pdf');
    }
}
