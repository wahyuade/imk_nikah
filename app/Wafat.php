<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wafat extends Model
{ 
	protected $fillable = [
	'id', 'nama', 'wali', 'tmpt_lahir', 'tgl_lahir', 'tgl_meninggal', 'tmpt_meninggal', 'warganegara', 'agama', 'pekerjaan', 'alamat',
	];
	public $timestamps = false;
}
