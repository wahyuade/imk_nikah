<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keterangan extends Model
{
    protected $fillable = [ 
    	'id','username', 'hari', 'tgl', 'jam', 'mas_kawin', 'bayar_mas_kawin', 'tmpt', 'nama_penerima_brks', 'tgl_terima',
    ];
    public $timestamps = false;
}
