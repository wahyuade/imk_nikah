-- phpMyAdmin SQL Dump
-- version 4.6.4deb1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 11, 2016 at 05:08 PM
-- Server version: 10.0.25-MariaDB-1
-- PHP Version: 7.0.8-3ubuntu3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nikah`
--

-- --------------------------------------------------------

--
-- Table structure for table `ayahs`
--

CREATE TABLE `ayahs` (
  `id` int(11) NOT NULL,
  `nama` text NOT NULL,
  `tmpt_lahir` text NOT NULL,
  `tgl_lahir` date NOT NULL,
  `warganegara` text NOT NULL,
  `agama` text NOT NULL,
  `pekerjaan` text NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ibus`
--

CREATE TABLE `ibus` (
  `id` int(11) NOT NULL,
  `nama` text NOT NULL,
  `tmpt_lahir` text NOT NULL,
  `tgl_lahir` date NOT NULL,
  `warganegara` text NOT NULL,
  `agama` text NOT NULL,
  `pekerjaan` text NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `keterangans`
--

CREATE TABLE `keterangans` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `hari` text NOT NULL,
  `tgl` date NOT NULL,
  `jam` text NOT NULL,
  `mas_kawin` text NOT NULL,
  `bayar_mas_kawin` text NOT NULL,
  `tmpt` text NOT NULL,
  `nama_penerima_brks` text NOT NULL,
  `tgl_terima` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` text NOT NULL,
  `nama` text NOT NULL,
  `wali` text,
  `username_calon` varchar(20) DEFAULT NULL,
  `jenis_kelamin` text NOT NULL,
  `agama` text NOT NULL,
  `pekerjaan` text NOT NULL,
  `tmpt_lahir` text NOT NULL,
  `tgl_lahir` date NOT NULL,
  `warganegara` text NOT NULL,
  `desa_kelurahan` text NOT NULL,
  `kecamatan` text NOT NULL,
  `kab_kota` text NOT NULL,
  `kepala_ds_kel` text NOT NULL,
  `alamat` text,
  `status` text,
  `mantan` text,
  `remember_token` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wafats`
--

CREATE TABLE `wafats` (
  `id` int(11) NOT NULL,
  `nama` text NOT NULL,
  `wali` text NOT NULL,
  `tmpt_lahir` text NOT NULL,
  `tgl_lahir` date NOT NULL,
  `tgl_meninggal` text NOT NULL,
  `tmpt_meninggal` text NOT NULL,
  `warganegara` text NOT NULL,
  `agama` text NOT NULL,
  `pekerjaan` text NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ayahs`
--
ALTER TABLE `ayahs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ibus`
--
ALTER TABLE `ibus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keterangans`
--
ALTER TABLE `keterangans`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `username_calon` (`username_calon`);

--
-- Indexes for table `wafats`
--
ALTER TABLE `wafats`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ayahs`
--
ALTER TABLE `ayahs`
  ADD CONSTRAINT `ayahs_ibfk_1` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ibus`
--
ALTER TABLE `ibus`
  ADD CONSTRAINT `ibus_ibfk_1` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `wafats`
--
ALTER TABLE `wafats`
  ADD CONSTRAINT `wafats_ibfk_1` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
