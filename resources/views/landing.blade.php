<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Kementrian Agama Republik Indonesia</title>
  <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
      <link rel="stylesheet" href="data/css/style.css">  
</head>
<body>
  <div class="form">
      <ul class="tab-group">
        <li class="tab"><a id="a" href="#pendaftaran">Daftar</a></li>
        <li class="tab"><a id="b" href="#login">Masuk</a></li>
      </ul>
      <div class="tab-content">
        <div id="pendaftaran" style="display: none;">   
          <h1>Pendaftaran Nikah</h1>
          <form action="/daftar" method="post">
          <div class="top-row">
            <div class="field-wrap">
                <label>
                  Nama Lengkap dan alias<span class="req">*</span>
                </label>
                <input type="text" name="nama" required />
            </div>
            <div class="field-wrap">
                <select name="jenis_kelamin" style="background-color: rgba(19, 35, 47,1)">
                    <option disabled selected>Jenis Kelamin :</option>
                    <option value="Laki-laki">Laki-Laki</option>
                    <option value="Perempuan">Perempuan</option>
                </select>
            </div>
          </div>
          <div class="top-row">
            <div class="field-wrap">
              <label>
                Agama<span class="req">*</span>
              </label>
              <input type="text" name="agama" required />
            </div> 
            <div class="field-wrap">
              <label>
                Pekerjaan<span class="req">*</span>
              </label>
              <input type="text" name="pekerjaan" required />
            </div> 
          </div>
          <div class="field-wrap">
            <label>
              Tempat Lahir<span class="req">*</span>
            </label>
            <input type="text" name="tmpt_lahir" required />
          </div>
          <div class="top-row">
            <div class="field-wrap" style="left: 13%;">
              <label>
                Tanggal Lahir<span class="req">*</span>
              </label>              
              <br>
            </div>
            <div class="field-wrap" style="right: 13%;">
              <input type="date" name="tgl_lahir" required dateformat="d M Y"/>
            </div>
          </div>
          <div class="field-wrap">
            <label>
              Warga Negara<span class="req">*</span>
            </label>
            <input type="text" name="warganegara" required />
          </div>
          <div class="top-row">
            <div class="field-wrap">
              <label>
                Nama pengguna<span class="req">*</span>
              </label>
              <input type="text" name="username" required />
            </div>
            <div class="field-wrap">
              <label>
                Kata sandi<span class="req">*</span>
              </label>
              <input type="password" name="password" required />
            </div>
          </div>
          <div class="field-wrap">
            <label>
              Kantor Desa / Kelurahan<span class="req">*</span>
            </label>
            <input type="text" name="desa_kelurahan" required />
          </div>
          <div class="field-wrap">
              <label>
                Kecamatan<span class="req">*</span>
              </label>
              <input type="text" name="kecamatan" required />
            </div>
          <div class="top-row">
            <div class="field-wrap">
              <select name="kab_kota" style="background-color: rgba(19, 35, 47,1)">
                  <option disabled selected>Kabupaten / Kota</option>
                  <option value="Kabupaten " style="color:#fff">Kabupaten</option>
                  <option value="Kota " style="color:#fff">Kota</option>
              </select>
            </div>
            <div class="field-wrap">
              <label>
                Nama Kabupaten / Kota<span class="req">*</span>
              </label>
              <input type="text" name="nama_kab_kota" required />
            </div>             
          </div>
          <div class="field-wrap">
              <label>
                Kepala Desa / Kelurahan<span class="req">*</span>
              </label>
              <input type="text" name="kepala_ds_kel" required/>
            </div>  
          <button type="submit" class="button button-block"/>Daftar Sekarang !</button>
          </form>
        </div>
        <div id="login">   
          <h1>Silahkan Masuk</h1>
          <form action="masuk" method="post">
            <div class="field-wrap">
            <label>
              Nama pengguna<span class="req">*</span>
            </label>
            <input name="username" type="text"required />
          </div>
          <div class="field-wrap">
            <label>
              Kata sandi<span class="req">*</span>
            </label>
            <input name="password" type="password"required />
          </div>
          <button class="button button-block"/>Masuk</button>
          </form>
        </div>
      </div><!-- tab-content -->
</div> <!-- /form -->
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="data/js/index.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
          <?php 
              echo $tab;
           ?>
        });
    </script>
</body>
</html>
