<!-- multistep form -->
<link rel="stylesheet" type="text/css" href="style.css">
<div id="msform">
	<!-- progressbar -->
	<ul id="progressbar">
		<li class="active">Data Diri / Riwayat Pernikahan</li>
		<li>Asal-Usul Ayah</li>
		<li>Asal-Usul Ibu</li>
		<li>Calon Mempelai</li>
		<li>Keterangan Kematian Suami/Istri</li>
		<li>Keterangan Pernikahan</li>
		<li>Cetak / <a href="keluar" style="text-decoration: none;color: #fff">Keluar</a></li>
	</ul>
	<!-- fieldsets -->
	<fieldset> <!-- Nomer 1 -->
		<h2 class="fs-title">Surat Keterangan Nikah</h2>
		<h3 class="fs-subtitle">Data diri Nikah atas nama <b>{{Auth::User()->nama}}</b></h3>
		<div id="tab_input">
		<form id="1" onkeyup="form_1()">
			<input type="text" name="wali" placeholder="@if(Auth::User()->jenis_kelamin != "Perempuan")Bin
			@else
Binti
			@endif
			" required value="{{Auth::User()->wali}}" />
			<select id="ex_c" onchange="status();form_1();" name="status_kawin" required> <!-- ifffff -->
			@if(Auth::User()->status == NULL)
				<option selected disabled>Status Perkawinan :</option>
				@if(Auth::User()->jenis_kelamin == "Perempuan")
					<option value="Perawan">Perawan</option>
					<option value="Bersuami">Bersuami</option>
					<option value="Janda">Janda</option>
				@else
				<option value="Jejaka">Jejaka</option>
				<option value="Beristri">Beristri</option>
				<option value="Duda">Duda</option> <!-- ifffff -->
				@endif
			@elseif(Auth::User()->status == "Perawan")
				<option selected value="Perawan">Perawan</option>
			@elseif(Auth::User()->status == "Bersuami")
				<option selected value="Bersuami">Bersuami</option>
			@elseif(Auth::User()->status == "Janda")
				<option selected value="Janda">Janda</option>
			@elseif(Auth::User()->status == "Jejaka")
				<option selected value="Jejaka">Jejaka</option>
			@elseif(Auth::User()->status == "Beristri")
				<option selected value="Beristri">Beristri</option>
			@elseif(Auth::User()->status == "Duda")
				<option selected value="Duda">Duda</option>
			@endif
			</select>
		</div>
		<div id="ex_div"></div>
		<input onkeyup="form_1()" type="text" name="alamat" placeholder="Alamat" required value="{{Auth::User()->alamat}}" />
		<input id="ex_s" type="text" name="nama_pasangan_ex" hidden>
		@if(Auth::User()->wali == NULL)
		<input id="button_1" type="submit" name="next" class="next action-button" value="Selanjutnya" style="display: none" />
		@else
		<input type="button" name="next" class="next action-button" value="Selanjutnya"/>
		@endif
		</form>
	</fieldset>
	<fieldset> <!-- Nomer 2 -->
		<h2 class="fs-title">Asal-Usul Ayah</h2>		
		<h3 class="fs-subtitle">Nama Ayah Kandung dari <b>{{Auth::User()->nama}}</b> :</h3>
		<div>		
			<form id="2" onkeyup="form_2()">
				<input type="text" onkeyup="form_2()" name="nama_ayah" placeholder="Nama Lengkap dan alias" value="{{$ayah["nama"]}}" />
				<div id="tab_input">
					<input type="text" onkeyup="form_2()" name="tmpt_lahir_ayah" placeholder="Tempat dan tanggal Lahir" value="{{$ayah["tmpt_lahir"]}}"/>
					<input type="date" onkeyup="form_2()" name="tgl_lahir_ayah" value="{{$ayah["tgl_lahir"]}}"/>
				</div>
				</div>
				<input type="text" onkeyup="form_2()" name="warganegara_ayah" placeholder="Warga Negara" value="{{$ayah["warganegara"]}}" />
				<div id="tab_input">
					<input type="text" onkeyup="form_2()" name="agama_ayah" placeholder="Agama" value="{{$ayah["agama"]}}"/>
					<input type="text" onkeyup="form_2()" name="pekerjaan_ayah" placeholder="Pekerjaan" value="{{$ayah["pekerjaan"]}}" />
				</div>
				<input type="text" onkeyup="form_2()" name="alamat_ayah" placeholder="Alamat" value="{{$ayah["alamat"]}}"/>
				<input type="button" name="previous" class="previous action-button" value="Sebelumnya" />
				@if($ayah == NULL)
				<input id="button_2" type="submit" name="next" class="next action-button" value="Selanjutnya" style="display: none" />
				@else
				<input type="button" name="next" class="next action-button" value="Selanjutnya" />
				@endif
			</form>
	</fieldset>
	<fieldset> <!-- Nomer 3 -->
		<h2 class="fs-title">Asal-Usul Ibu</h2>
		<h3 class="fs-subtitle">Nama Ibu Kandung dari <b>{{Auth::User()->nama}}</b> :</h3>
		<div>
		<form id="3" onkeyup="form_3()">
			<input type="text" onkeyup="form_3()" name="nama_ibu" placeholder="Nama Lengkap dan alias" value="{{$ibu["nama"]}}" />
			<div id="tab_input">
				<input type="text" onkeyup="form_3()" name="tmpt_lahir_ibu" placeholder="Tempat dan tanggal Lahir" value="{{$ibu["tmpt_lahir"]}}" />
				<input type="date" onkeyup="form_3()" name="tgl_lahir_ibu" value="{{$ibu["tgl_lahir"]}}"/>
			</div>
			<input type="text" onkeyup="form_3()" name="warganegara_ibu" placeholder="Warga Negara" value="{{$ibu["warganegara"]}}"/>
			</div>
			<div id="tab_input">
				<input type="text" onkeyup="form_3()" name="agama_ibu" placeholder="Agama" value="{{$ibu["agama"]}}" />
				<input type="text" onkeyup="form_3()" name="pekerjaan_ibu" placeholder="Pekerjaan" value="{{$ibu["pekerjaan"]}}"/>
			</div>
			<input type="text" onkeyup="form_3()" name="alamat_ibu" placeholder="Alamat" value="{{$ibu["alamat"]}}"/>
			<input type="button" name="previous" class="previous action-button" value="Sebelumnya" />
			@if($ibu == NULL)
			<input id="button_3" type="submit" name="next" class="next action-button" value="Selanjutnya" style="display: none" />
			@else
			<input type="button" name="next" class="next action-button" value="Selanjutnya" />
			@endif
		</form>
	</fieldset>
	<fieldset> 
		<h2 class="fs-title">Calon Mempelai</h2>
		@if($calon == NULL)
		<h3 class="fs-subtitle">Masukkan <i>username</i> mempelai anda :</h3>
		<div>
		<form id="4" onkeydown="form_4()">
		<input type="text" onkeydown="form_4()" name="username_calon" placeholder="Username calon mempelai"/>
		</div>
		<p id="tes_username" class="fs-subtitle" style="font-size: 15px">*Apabila calon mempelai anda belum daftar, silahkan daftarkan terlebih dahulu. Username dapat diperoleh ketika mendaftar di <a href="/" target="_blank" style="text-decoration-style: none; color: white;">HALAMAN DAFTAR</a></p>
		<input type="button" name="previous" class="previous action-button" value="Sebelumnya" />
		<input id="button_4" type="submit" name="next" class="next action-button" value="Selanjutnya" style="display: none"/>
		@else
		<h3 class="fs-subtitle">Nama Calon mempelai anda adalah:</h3>
		<div>
		<form >
		<input type="text" name="username_calon" placeholder="Username calon mempelai" value="{{$calon["nama_mempelai"]}}" disabled />
		</div>
		<input type="button" name="previous" class="previous action-button" value="Sebelumnya" />
		<input type="button" name="next" class="next action-button" value="Selanjutnya"/>
		@endif
		</form>
	</fieldset>
	<fieldset> <!-- Nomer 5 -->
	@if(Auth::User()->status == "Duda" || Auth::User()->status == "Janda")
		<h2 class="fs-title">Keterangan Kematian Suami/Istri</h2>
		<h3 class="fs-subtitle">Data diri 
@if(Auth::User()->jenis_kelamin == "Perempuan")
almarhum suami
@else
almarhumah istri
@endif
dari <b>{{Auth::User()->nama}}</b></h3>
		<div>
		<form id="5" onkeyup="form_5()">
		<input type="text" onkeyup="form_5()" name="nama_wafat" placeholder="Nama Lengkap dan alias" />
		<input type="text" onkeyup="form_5()" name="wali_wafat" placeholder="Bin / Binti" />
		<div id="tab_input">
			<input type="text" onkeyup="form_5()" name="tmpt_lahir_wafat" placeholder="Tempat Lahir" />
			<input type="date" onkeyup="form_5()" name="tgl_lahir_wafat" placeholder="Tanggal Lahir" />
		</div>
		</div>
		<div id="tab_input">
			<input type="text" onkeyup="form_5()" name="tmpt_meninggal_wafat" placeholder="Tempat Meninggal" />
			<input type="date" onkeyup="form_5()" name="tgl_meninggal_wafat" placeholder="Tanggal Meninggal" />
		</div>
		<input type="text" onkeyup="form_5()" name="warganegara_wafat" placeholder="Warga Negara" />
		<div id="tab_input">
			<input type="text" onkeyup="form_5()" name="agama_wafat" placeholder="Agama" />
			<input type="text" onkeyup="form_5()" name="pekerjaan_wafat" placeholder="Pekerjaan Terakhir" />
		</div>
		<input type="text" onkeyup="form_5()" name="alamat_wafat" placeholder="Alamat Terakhir" />
		<input id="button_5_kembali" type="button" name="previous" class="previous action-button" value="Sebelumnya" />
		<input id="button_5" type="submit" name="next" class="next action-button" value="Selanjutnya" style="display: none" />
	@else
		<h2 class="fs-title">Keterangan kematian hanya untuk yang berstatus Janda atau Duda</h2>
		<h3 class="fs-subtitle" style="text-align: center;">Lewati step ini</h3>
		<input id="button_5_kembali" type="button" name="previous" class="previous action-button" value="Sebelumnya" />
		<input type="button" name="next" class="next action-button" value="Selanjutnya" />
	@endif
		</form>
	</fieldset>
	<fieldset> <!-- Nomer 6 -->
		<h2 class="fs-title">Keterangan Pernikahan</h2>
		<div>
		<form id="6" onkeypress="form_6()">
		<div id="tab_input">
			<input type="text" onkeypress="form_6()" name="hari_nikah" placeholder="Hari Pernikahan" value="{{$keterangan["hari"]}}" />
			<input type="date" onkeypress="form_6()" name="tanggal_nikah" placeholder="Tanggal Pernikahan" value="{{$keterangan["tgl"]}}"/>
		</div>
		</div>
		<input type="text" onkeypress="form_6()" name="jam_nikah" placeholder="Jam Pernikahan" value="{{$keterangan["jam"]}}" />
		<div id="tab_input">
			<input type="text" onkeypress="form_6()" name="mas_kawin" placeholder="Mas Kawin" value="{{$keterangan["mas_kawin"]}}" />
			<select name="bayar_mas_kawin" onkeypress="form_6()">
			@if($keterangan == NULL)
				<option selected disabled>Tunai / Hutang</option>
				<option value="Tunai">Tunai</option>
				<option value="Hutang">Hutang</option>
			@elseif($keterangan["bayar_mas_kawin"] == "Tunai")
				<option selected value="Tunai">Tunai</option>
			@elseif($keterangan["bayar_mas_kawin"] == "Hutang")
				<option selected value="Hutang">Hutang</option>
			@endif
			</select>
		</div>
		<input type="text" onkeypress="form_6()" name="tempat_nikah" placeholder="Tempat Pernikahan" value="{{$keterangan["tmpt"]}}"/>
		<input type="text" onkeypress="form_6()" name="penerima_berkas_nikah" placeholder="Penerima Berkas Pernikahan" value="{{$keterangan["nama_penerima_brks"]}}"/>
		<input type="button" name="previous" class="previous action-button" value="Sebelumnya" />
		@if($keterangan == NULL)
		<input id="button_6" type="submit" class="next action-button" value="Kirim" style="display: none" />
		@else
		<input id="button_6" type="button" class="next action-button" value="Kirim"/>
		@endif

		</form>
	</fieldset>
	<fieldset> 
	@if($data_calon["wali"] != NULL && $data_ayah_calon["nama"] != NULL && $data_ibu_calon["nama"] != NULL)
		<h2 class="fs-title">Siap Cetak</h2>
		<h3 class="fs-subtitle">Berkas yang dapat dicetak :</h3>
		<ol id="cetak">
			<li><a target="_blank" href="fp" style="text-decoration: none;
	color: #fff;">Formulir Pernikahan</a></li>
			<li><a target="_blank" href="skm" style="text-decoration: none;
	color: #fff;">Surat Keterangan untuk Menikah</a></li>
			<li><a target="_blank" href="ska" style="text-decoration: none;
	color: #fff;">Surat Keterangan Asal Usul</a></li>
			<li><a target="_blank" href="skc" style="text-decoration: none;
	color: #fff;">Surat Persetujuan Mempelai</a></li>
			<li><a target="_blank" href="sko" style="text-decoration: none;
	color: #fff;">Surat Keterangan Tentang Orang Tua</a></li>
			<li><a target="_blank" href="sio" style="text-decoration: none;
	color: #fff;">Surat Ijin Orang Tua</a></li>
			<li><a target="_blank" href="skk" style="text-decoration: none;
	color: #fff;">Surat Keterangan Kematian Pasangan Sebelumnya</a></li>
		</ol>
	@else
		<h2 class="fs-title">Mohon Maaf</h2>
		<h3 class="fs-subtitle" style="font-size: 13px;text-align: center;">Berkas anda belum dapat dicetak, karena pasangan anda ({{$data_calon["nama"]}}) belum melengkapi data. Silahkan melengkapi data pasangan anda dengan username <b>{{$data_calon["username"]}}</b> dengan menekan tombol<a href="keluar" style="text-decoration-style: none;color: green;"> keluar</a> terlebih dahulu. Terima Kasih.</h3>
		<p style="color: silver;margin-bottom: 10px;">Data yang belum dilengkapi :</p>
		<ul style="color: white; text-align: left;margin-left: 20%;">
			@if($data_calon["wali"] == NULL)
				<li>Data Diri / Riwayat Pernikahan dari {{$data_calon["nama"]}}</li>
			@endif
			@if($data_ayah_calon["nama"] == NULL)
				<li>Asal - Usul Ayah dari {{$data_calon["nama"]}}</li>
			@endif
			@if($data_ibu_calon["nama"] == NULL)
				<li>Asal - Usul Ibu dari {{$data_calon["nama"]}}</li>
			@endif
		</ul>
	@endif
	</fieldset>
</div>

<!-- jQuery -->
<script src='jquery.min.js'></script>
<script type="text/javascript">
	function form_1(){
		if(document.getElementById('1')[0].value != "" && document.getElementById('1')[1].value != "Status Perkawinan :" && document.getElementById('1')[2].value != ""){
			if(document.getElementById('ex_c').value == "Janda" || document.getElementById('ex_c').value == "Duda"){
				if(document.getElementById('ex').value == ""){
					document.getElementById('button_1').style.display = "none";
					document.getElementById('ex_s').value = document.getElementById('ex').value;
				}
				else{
					document.getElementById('button_1').style.display = "";	
				}
			}
			else{
				document.getElementById('button_1').style.display = "";
			}
		}else{
			document.getElementById('button_1').style.display = "none";
		}
	}
	function form_2(){
		if(document.getElementById('2')[0].value != "" && document.getElementById('2')[1].value != "" && document.getElementById('2')[2].value != "" && document.getElementById('2')[3].value != "" && document.getElementById('2')[4].value != "" && document.getElementById('2')[5].value != "" && document.getElementById('2')[6].value != ""){
			document.getElementById('button_2').style.display = "";
		}else{
			document.getElementById('button_2').style.display = "none";
		}
	}
	function form_3(){
		if(document.getElementById('3')[0].value != "" && document.getElementById('3')[1].value != "" && document.getElementById('3')[2].value != "" && document.getElementById('3')[3].value != "" && document.getElementById('3')[4].value != "" && document.getElementById('3')[5].value != "" && document.getElementById('3')[6].value != ""){
			document.getElementById('button_3').style.display = "";
		}else{
			document.getElementById('button_3').style.display = "none";
		}
	}
	function form_4(){
		if(document.getElementById('4')[0].value != ""){
			document.getElementById('button_4').style.display = "";
		}else{
			document.getElementById('button_4').style.display = "none";
		}
	}
	function form_5(){
		if(document.getElementById('5')[0].value != "" && document.getElementById('5')[1].value != "" && document.getElementById('5')[2].value != "" && document.getElementById('5')[3].value != "" && document.getElementById('5')[4].value != "" && document.getElementById('5')[5].value != "" && document.getElementById('5')[6].value != "" && document.getElementById('5')[7].value != "" && document.getElementById('5')[8].value != "" && document.getElementById('5')[9].value != ""){
			document.getElementById('button_5').style.display = "";
		}else{
			document.getElementById('button_5').style.display = "none";
		}
	}
	function form_6(){
		if(document.getElementById('6')[0].value != "" && document.getElementById('6')[1].value != "" && document.getElementById('6')[2].value != "" && document.getElementById('6')[3].value != "" && document.getElementById('6')[4].value != "" && document.getElementById('6')[5].value != "" && document.getElementById('6')[6].value != ""){
			document.getElementById('button_6').style.display = "";
		}else{
			document.getElementById('button_6').style.display = "none";
		}
	}
	function status(){
		if(document.getElementById('ex_c').value == "Janda")
			document.getElementById('ex_div').innerHTML = '<input id="ex" onkeyup="form_1()" type="text"  placeholder="Nama Suami terdahulu" required>';
		else if(document.getElementById('ex_c').value == "Duda"){
			document.getElementById('ex_div').innerHTML = '<input id="ex" onkeyup="form_1()" type="text"  placeholder="Nama Istri terdahulu" required>';
		}
		else{
			$("#ex").remove();
		}
	}
	
	$("#1").submit(function(e) {
	    var url = "1"; // the script where you handle the form input.
	    $.ajax({
	           type: "POST",
	           url: url,
	           data: $("#1").serialize(), // serializes the form's elements.
	           success: function(data)
	           {
	           		console.log(data);
	           }
	         });
	    e.preventDefault(); // avoid to execute the actual submit of the form.
	});
	$("#2").submit(function(e) {
	    var url = "2"; // the script where you handle the form input.
	    $.ajax({
	           type: "POST",
	           url: url,
	           data: $("#2").serialize(), // serializes the form's elements.
	           success: function(data)
	           {
	           		console.log(data);
	           }
	         });
	    e.preventDefault(); // avoid to execute the actual submit of the form.
	});
	$("#3").submit(function(e) {
	    var url = "3"; // the script where you handle the form input.
	    $.ajax({
	           type: "POST",
	           url: url,
	           data: $("#3").serialize(), // serializes the form's elements.
	           success: function(data)
	           {
	           		console.log(data);
	           }
	         });
	    e.preventDefault(); // avoid to execute the actual submit of the form.
	});
	$("#4").submit(function(e) {
	    var url = "4"; // the script where you handle the form input.
	    $.ajax({
	           type: "POST",
	           url: url,
	           data: $("#4").serialize(), // serializes the form's elements.
	           success: function(data)
	           {
	           		if(data.status){
	           			document.getElementById('tes_username').innerHTML = '<span style="color:green;">Username '+data.username+' sudah berhasil dimasukkan.</span>';
	           		}
	           		else{
	           			document.getElementById('tes_username').innerHTML = '<span style="color:red;">Mohon maaf username <span style="color:white;">'+data.username+'</span> belum terdaftar.</span><br>Username dapat diperoleh ketika mendaftar di <a href="/" target="_blank" style="text-decoration-style: none; color: white;">HALAMAN DAFTAR</a>';
	           				setTimeout(function(){ 
	           					document.getElementById("button_5_kembali").click();
	           				}, 900);
	           		}
	           }
	         });
	    e.preventDefault(); // avoid to execute the actual submit of the form.
	});
	$("#5").submit(function(e) {
	    var url = "5"; // the script where you handle the form input.
	    $.ajax({
	           type: "POST",
	           url: url,
	           data: $("#5").serialize(), // serializes the form's elements.
	           success: function(data)
	           {
	           		console.log(data);
	           }
	         });
	    e.preventDefault(); // avoid to execute the actual submit of the form.
	});
	$("#6").submit(function(e) {
	    var url = "6"; // the script where you handle the form input.
	    $.ajax({
	           type: "POST",
	           url: url,
	           data: $("#6").serialize(), // serializes the form's elements.
	           success: function(data)
	           {
	           		console.log(data);
	           }
	         });
	    e.preventDefault(); // avoid to execute the actual submit of the form.
	});
</script>
<!-- jQuery easing plugin -->
<script src="jquery.easing.min.js" type="text/javascript"></script>
<script type="text/javascript" src="script.js"></script>
